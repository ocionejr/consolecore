using GameTOP.Interface;

namespace GameTOP.lib
{
    public class Jogador2 : iJogador
    {
        public string Chuta()
        {
            return "Neymar ta chutando \n";
        }

        public string Corre()
        {
            return "Neymar ta correndo \n";
        }

        public string Passa()
        {
            return "Neymar ta passando \n";
        }
    }
}