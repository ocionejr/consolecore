using System;
using GameTOP.Interface;

namespace GameTOP.lib
{
    public class Jogador : iJogador
    {
        public string _nome { get; set; }

        public Jogador(string nome = "Ronaldo")
        {
            this._nome = nome;
        }

        public string Chuta()
        {
            return $"{_nome} está chutando \n";
            
        }

        public string Corre()
        {
            return $"{_nome} está correndo \n";
        }

        public string Passa()
        {
            return $"{_nome} está passando \n";
        }
    }
}