using GameTOP.Interface;

namespace GameTOP
{
    public class JogoFODA{
        private readonly iJogador _jogador;
        private readonly iJogador _jogador2;

        public JogoFODA(iJogador jogador1, iJogador jogador2){
            this._jogador = jogador1;
            this._jogador2 = jogador2;
        }

        public void IniciarJogo()
        {
            System.Console.Write(_jogador.Corre());
            System.Console.Write(_jogador.Chuta());
            System.Console.Write(_jogador.Passa());

            System.Console.Write(_jogador2.Corre());
            System.Console.Write(_jogador2.Chuta());
            System.Console.Write(_jogador2.Passa());
        }
    }
}